#include <stdio.h>
#include "bir.h"
#include "bi_builder.h"
#include "bi_printer.h"
#include "util/set.h"
#include "util/hash_table.h"

static bi_block *
bi_create_block(bi_shader *ctx)
{
        bi_block *blk = rzalloc(ctx, bi_block);

#ifdef IS_MESA
        blk->predecessors = _mesa_set_create(blk,
                _mesa_hash_pointer,
                _mesa_key_pointer_equal);
#endif

        list_inithead(&blk->instructions);

        return blk;
}

int main(int argc, char **argv)
{
    bi_shader _shader;
    bi_shader *shader = &_shader;

    util_dynarray_init(&shader->defs, NULL);

    bi_block *block = bi_create_block(shader);
    list_inithead(&shader->blocks);
    list_addtail(&block->link, &shader->blocks);

    bi_builder _b = {
        .shader = shader,
        .cursor = bi_after_block(block)
    };

    bi_builder *b = &_b;

//    bi_eureka(b, bi_barrier(b));

    printf("Hello, world!");
}
