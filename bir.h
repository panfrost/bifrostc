/*
 * Copyright (C) 2020 Collabora Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Authors (Collabora):
 *      Alyssa Rosenzweig <alyssa.rosenzweig@collabora.com>
 */

#ifndef __BIR_H
#define __BIR_H

#include "bifrost.h"
#include "bi_opcodes.h"
#include "util/list.h"
#include "util/u_dynarray.h"

#ifndef IS_MESA
#undef rzalloc
#define rzalloc(ctx, T) calloc(sizeof(T), 1)
#endif

typedef uint32_t bi_index;

#define BI_MAX_DESTS 2
#define BI_MAX_SRCS 4

/* All of these can act as a swizzle in some context, the instruction in
 * question matters to interpret. Unioned here for easy manipulation */

typedef union {
    enum bi_lane lane;
    enum bi_lanes lanes;
    enum bi_replicate replicate;
    enum bi_swz swz;
    enum bi_widen widen;
} bi_swizzle;

typedef struct {
    /* Must be first */
    struct list_head link;

    /* Link for the use chain */
    struct list_head use;

    enum bi_opcode op;

    /* Data flow */
    bi_index dest[BI_MAX_DESTS];
    bi_index src[BI_MAX_SRCS];

    /* Everything after this MUST NOT be accessed directly, since
     * interpretation depends on opcodes */

    /* Swizzles of various kinds */
    bi_swizzle swizzle[BI_MAX_SRCS];

    /* Source modifiers */
    union {
        struct {
            bool abs[BI_MAX_SRCS];
            bool neg[BI_MAX_SRCS];
        };

        bool not[BI_MAX_SRCS];
        bool swap[BI_MAX_SRCS];
        bool sign[BI_MAX_SRCS];
    };

    /* Destination modifiers */
    union {
        enum bi_clamp clamp;
        bool saturate;
        bool not_result;
    };

    /* These don't fit neatly with anything else.. */
    enum bi_register_format register_format;
    enum bi_vecsize vecsize;

    /* Immediates. All seen alone in an instruction, except for varying/texture
     * which are specified jointly for VARTEX */
    union {
        uint32_t shift;
        uint32_t fill;
        uint32_t index;
        uint32_t table;
        uint32_t attribute_index;
        
        struct {
            uint32_t varying_index;
            uint32_t sampler_index;
            uint32_t texture_index;
        };
    };

    /* Modifiers specific to particular instructions are thrown in a union */
    union {
        enum bi_adj adj; /* FEXP_TABLE.u4 */
        enum bi_atom_opc atom_opc; /* atomics */
        enum bi_func func; /* FPOW_SC_DET */
        enum bi_function function; /* LD_VAR_FLAT */
        enum bi_mode mode; /* FLOG_TABLE */
        enum bi_mux mux; /* MUX */
        enum bi_precision precision; /* FLOG_TABLE */
        enum bi_sem sem; /* FMAX, FMIN */
        enum bi_source source; /* LD_GCLK */
        bool scale; /* VN_ASST2, FSINCOS_OFFSET */
        bool offset; /* FSIN_TABLE, FOCS_TABLE */
        bool divzero; /* FRSQ_APPROX, FRSQ */
        bool mask; /* CLZ */
        bool threads; /* IMULD, IMOV_FMA */
        bool combine; /* BRANCHC */
        bool format; /* LEA_TEX */

        struct {
            bool skip; /* VAR_TEX, TEXS */
            bool lod_mode; /* TEXS */
        };

        struct {
            enum bi_special special; /* FADD_RSCALE, FMA_RSCALE */
            enum bi_round round; /* FMA, converts, FADD, _RSCALE, etc */
        };

        struct {
            enum bi_result_type result_type; /* FCMP, ICMP */
            enum bi_cmpf cmpf; /* CSEL, FCMP, ICMP, BRANCH */
        };

        struct {
            enum bi_stack_mode stack_mode; /* JUMP_EX */
            bool test_mode;
        };

        struct {
            enum bi_seg seg; /* LOAD, STORE, SEG_ADD, SEG_SUB */
            bool preserve_null; /* SEG_ADD, SEG_SUB */
            enum bi_extend extend; /* LOAD, IMUL */
        };

        struct {
            enum bi_sample sample; /* LD_VAR */
            enum bi_update update; /* LD_VAR */
            enum bi_varying_name varying_name; /* LD_VAR_SPECIAL */
        };

        struct {
            enum bi_subgroup subgroup; /* WMASK, CLPER */
            enum bi_inactive_result inactive_result; /* CLPER */
            enum bi_lane_op lane_op; /* CLPER */
        };

        struct {
            bool z; /* ZS_EMIT */
            bool stencil; /* ZS_EMIT */
        };

        struct {
            bool h; /* VN_ASST1.f16 */
            bool l; /* VN_ASST1.f16 */
        };

        struct {
            bool bytes2; /* RROT_DOUBLE, FRSHIFT_DOUBLE */
            bool result_word;
        };

        struct {
            bool sqrt; /* FREXPM */
            bool log; /* FREXPM */
        };
    };
} bi_instr;

typedef struct {
    /* If SSA, the unique instruction producing this. If
     * not SSA, left NULL. */
    bi_instr *parent;

    /* Set of bi_instr that read this */
    struct list_head uses;
} bi_def;

/* Represents the assignment of slots for a given bi_bundle */

typedef struct {
    /* Register to assign to each slot */
    unsigned slot[4];

    /* 0-2 read slots */
    unsigned read_slots;

    /* Configuration for slots 2/3 */
    struct bifrost_reg_ctrl_23 slot23;

    /* Whether writes are actually for the last instruction */
    bool first_instruction;
} bi_registers;

/* A bi_tuple contains two paired instruction pointers. If a unit is unfilled,
 * leave it NULL; the emitter will fill in a nop. Instructions reference
 * registers via slots which are assigned per tuple.
 */

typedef struct {
    uint8_t fau_idx;
    bi_registers regs;
    bi_instr *fma;
    bi_instr *add;
} bi_tuple;

typedef struct {
    struct list_head link;

    /* Link back up for branch calculations */
    struct bi_block *block;

    /* A clause can have no more than 8 tuples */
    unsigned tuple_count;
    bi_tuple tuples[8];

    /* For scoreboarding -- the clause ID (this is not globally unique!)
     * and its dependencies in terms of other clauses, computed during
     * scheduling and used when emitting code. Dependencies expressed as a
     * bitfield matching the hardware, except shifted by a clause (the
     * shift back to the ISA's off-by-one encoding is worked out when
     * emitting clauses) */
    unsigned scoreboard_id;
    uint8_t dependencies;

    /* See ISA header for description */
    enum bifrost_flow flow_control;

    /* Can we prefetch the next clause? Usually it makes sense, except for
     * clauses ending in unconditional branches */
    bool next_clause_prefetch;

    /* Assigned data register */
    unsigned staging_register;

    /* Corresponds to the usual bit but shifted by a clause */
    bool staging_barrier;

    /* Constants read by this clause. ISA limit. Must satisfy:
     *
     *      constant_count + bundle_count <= 13
     *
     * Also implicitly constant_count <= bundle_count since a bundle only
     * reads a single constant.
     */
    uint64_t constants[8];
    unsigned constant_count;

    /* There is at most one message-passing instruction in the clause. This
     * specifies its type */
    enum bifrost_message_type message_type;
} bi_clause;

typedef struct bi_block {
    /* Must be first */
    struct list_head link;

    /* List of bi_instr in the current block */
    struct list_head instructions;

    /* Control flow graph */
    struct bi_block *successors[2];
    struct set *predecessors;
} bi_block;

typedef struct {
#ifdef IS_MESA
    nir_shader *nir;
    gl_shader_stage stage;
    struct panfrost_sysvals sysvals;
#endif

    /* List of bi_block */
    struct list_head blocks; 

    /* Maps bi_index to bi_def */
    struct util_dynarray defs;

    uint32_t quirks;
    unsigned arch;
    unsigned tls_size;

    /* Is internally a blend shader? Depends on stage == FRAGMENT */
    bool is_blend;

    /* Blend constants */
    float blend_constants[4];

    /* Blend return offsets */
    uint32_t blend_ret_offsets[8];

    /* Blend tile buffer conversion desc */
    uint64_t blend_desc;

    /* For creating temporaries */
    unsigned temp_alloc;

    /* Analysis results */
    bool has_liveness;

    /* Stats for shader-db */
    unsigned instruction_count;
    unsigned loop_count;
    unsigned spills;
    unsigned fills;
} bi_shader;

#define BI_IS_REG (0x1)

static inline bi_def *
bi_add_def(bi_shader *ctx, bi_index index)
{
    /* Ensure there is space */
    util_dynarray_resize(&ctx->defs, bi_def, index + 1);

    bi_def *def = util_dynarray_element(&ctx->defs, bi_def, index);
    list_inithead(&def->uses);
    return def;
}

static inline bi_index
bi_make_temp(bi_shader *ctx, bi_instr *parent)
{
#ifdef IS_MESA
    bi_index index = (ctx->impl->ssa_alloc + 1 + ctx->temp_alloc++) << 1;
#else
    bi_index index = ((0)+ 1 + ctx->temp_alloc++) << 1;
#endif

    bi_def *def = bi_add_def(ctx, index);
    def->parent = parent;
    return index;
}

static inline bi_index
bi_make_temp_reg(bi_shader *ctx)
{
#ifdef IS_MESA
    bi_index index = ((ctx->impl->reg_alloc + ctx->temp_alloc++) << 1) | BI_IS_REG;
#else
    bi_index index = ((0) + 1 + ctx->temp_alloc++) << 1;
#endif

    bi_add_def(ctx, index);
    return index;
}

static void
bi_push_src(bi_shader *ctx, bi_instr *I, bi_index src)
{
    assert(src < util_dynarray_num_elements(&ctx->defs, bi_def));
    bi_def *def = util_dynarray_element(&ctx->defs, bi_def, src);

    /* Record the use */
    list_add(&I->use, &def->uses);
}

/* Like in NIR, for use with the builder */

enum bi_cursor_option {
    bi_cursor_after_block,
    bi_cursor_before_instr,
    bi_cursor_after_instr
};

typedef struct {
    enum bi_cursor_option option;

    union {
        bi_block *block;
        bi_instr *instr;
    };
} bi_cursor;

static inline bi_cursor
bi_after_block(bi_block *block)
{
    bi_cursor cursor = {
        .option = bi_cursor_after_block,
        .block = block
    };

    return cursor;
}

static inline bi_cursor
bi_before_instr(bi_instr *instr)
{
    bi_cursor cursor = {
        .option = bi_cursor_before_instr,
        .instr = instr
    };

    return cursor;
}

static inline bi_cursor
bi_after_instr(bi_instr *instr)
{
    bi_cursor cursor = {
        .option = bi_cursor_after_instr,
        .instr = instr
    };

    return cursor;
}

/* IR builder in terms of cursor infrastructure */

typedef struct {
    bi_shader *shader;
    bi_cursor cursor;
} bi_builder;

/* Insert an instruction at the cursor and move the cursor */

static inline void
bi_builder_insert(bi_cursor *cursor, bi_instr *I)
{
    switch (cursor->option) {
    case bi_cursor_after_instr:
        list_add(&I->link, &cursor->instr->link);
        cursor->instr = I;
        return;

    case bi_cursor_after_block:
        list_addtail(&I->link, &cursor->block->instructions);
        cursor->option = bi_cursor_after_instr;
        cursor->instr = I;
        return;

    case bi_cursor_before_instr:
        list_addtail(&I->link, &cursor->instr->link);
        cursor->option = bi_cursor_after_instr;
        cursor->instr = I;
    }

    unreachable("Invalid cursor option");
}

#endif
