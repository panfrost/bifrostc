import sys
import itertools
import copy
import math
from isa_parse import parse_instructions, opname_to_c
from mako.template import Template
from pprint import pprint

# TODO: dedupe
# Compiles a logic expression to Python expression, ctx -> { T, F }

EVALUATORS = {
        'and': ' and ',
        'or': ' or ',
        'eq': ' == ',
        'neq': ' != ',
}

def compile_derived_inner(expr, keys):
    if expr == []:
        return 'True'
    elif expr is None: #or expr[0] == 'alias':
        return 'False'
    elif expr[0] == 'alias':
        return compile_derived_inner(expr[1], keys)
    elif isinstance(expr, list):
        args = [compile_derived_inner(arg, keys) for arg in expr[1:]]
        return '(' + EVALUATORS[expr[0]].join(args) + ')'
    elif expr[0] == '#':
        return "'{}'".format(expr[1:])
    elif expr == 'ordering':
        return expr
    else:
        return "ctx[{}]".format(keys.index(expr))

def compile_derived(expr, keys):
    return eval('lambda ctx, ordering: ' + compile_derived_inner(expr, keys))

instructions = parse_instructions(sys.argv[1])

def check_ins(variants):
    # Accumulate modifiers across variants
    modifiers = {}

    for _, var in variants:
        for ((name, _, _), _, opts) in var['modifiers']:
            if name not in modifiers:
                modifiers[name] = set(opts)
            else:
                modifiers[name] |= set(opts)

    for mod in modifiers:
        modifiers[mod].discard('reserved')

    # Grab options
    keys = list(modifiers.keys())
    values = [list(modifiers[k]) for k in keys]

    # Precompile variant, derived selectors
    selectors = [compile_derived(v[0], keys) if v[0] else lambda x, y: True for v in variants]
    deriveds = [[[compile_derived(D, keys) if D else lambda x, y: False for D in d[1]] for d in V[1]["derived"]] for V in variants]
    swaps = [[compile_derived(d[1], keys) for d in V[1]["swaps"]] for V in variants]

    # Iterate all possible combinations
    for x in itertools.product(*values):
        # Constraint: DISCARD can't mix widening
        if ins == "DISCARD.f32" and x[keys.index("widen0")][0] != x[keys.index("widen1")][0]:
            continue

        if "update" in keys and "sample" in keys:
            update = x[keys.index("update")]
            sample = x[keys.index("sample")]

            # Constraint: Varying retrieve/none must be paired
            if (update == "retrieve") ^ (sample == "none"):
                continue

            # Constraint: Varying conditional must be center/centroid
            if (update == "conditional") and sample not in ["center", "centroid"]:
                continue

        if "lane" in keys and "extend" in keys:
            size = int(ins.split(".")[1][1:])
            base = ["b", "h", "w", "d"][int(math.log2(size / 8))]
            lane = x[keys.index("lane")]
            widening = lane[0] != base and lane != "none"

            # Constraint: extend is specified iff widening
            if widening ^ (x[keys.index("extend")] != "none"):
                continue

        # Constraint: fragz can't be loaded explicitly
        if ins == "LD_VAR_SPECIAL" and x[keys.index("sample")] == "explicit" and x[keys.index("varying_name")] == "frag_z":
            continue

        # Constraint: cube negation is tied
        if ins == "CUBEFACE1" and len(set([x[keys.index("neg0")], x[keys.index("neg1")], x[keys.index("neg2")]])) > 1:
            continue

        if ins in ["CUBE_SSEL", "CUBE_TSEL"] and len(set([x[keys.index("neg0")], x[keys.index("neg1")]])) > 1:
            continue

        # Constraint: *_RSCALE only supports modes from fixed transcendental sequences
        if ins in ["FMA_RSCALE.f32", "FADD_RSCALE.f32", "FMA_RSCALE.v2f16"]:
            clamp = x[keys.index("clamp")]
            special = x[keys.index("special")]
            roundm = x[keys.index("round")]

            if clamp != "none" and (special != "none" or roundm != "none"):
                continue

            if special != "n" and roundm != "none":
                continue

        # Constraint: FCMP.v2f16 can only negate one component
        if ins == "FCMP.v2f16" and x[keys.index("neg0")] == "neg" and x[keys.index("neg1")] == "neg":
            continue

        any_valid = False
        for i, valid in enumerate([s(x, False) for s in selectors]):
            if not valid:
                continue

            x_ = list(x)

            # Initial value arbitrary
            ordering = "gt"

            for j, swap in enumerate(swaps[i]):
                if not swap(x, ordering):
                    continue

                ((l, r), _, rewrite) = variants[i][1]["swaps"][j]

                # Constraint: two-abs requires distinct sources for FMIN.v2f16
                # XXX: This might not be quite right
                if l == 0 and r == 1:
                    assert(ordering == "gt")
                    ordering = "le"

                    if ins == "FMIN.v2f16":
                        ordering = "lt"

                for k, key in enumerate(keys):
                    # Only swap from the left so we don't double-swap
                    if key[-1] == str(l):
                        idx_l = keys.index(key[0:-1] + str(l))
                        idx_r = keys.index(key[0:-1] + str(r))

                        temp = x_[idx_l]
                        x_[idx_l] = x_[idx_r]
                        x_[idx_r] = temp

                for k in rewrite:
                    idx = keys.index(k)

                    if x_[idx] in rewrite[k]:
                        x_[idx] = rewrite[k][x_[idx]]
            if all([any([D(x_, ordering) for D in derived]) for derived in deriveds[i]]):
                any_valid = True
                break

        if not any_valid:
            print(ins, keys, x)

        #assert(any_valid)

#check_ins("+FADD.f32")
ins = "FMA.f32"
check_ins(instructions["*FMA.f32"])
for ins in set(k[1:] for k in instructions.keys()):
    FMA = instructions["*" + ins] if "*" + ins in instructions else []
    ADD = instructions["+" + ins] if "+" + ins in instructions else []
    check_ins(FMA + ADD)
